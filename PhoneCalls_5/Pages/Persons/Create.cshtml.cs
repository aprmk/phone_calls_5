using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PhoneCalls_5.Models;

namespace PhoneCalls_5.Pages.Persons
{
    public class CreateModel : PageModel
    {
        private readonly PhoneCalls_5.Models.PhoneCallsContext _context;

        public CreateModel(PhoneCalls_5.Models.PhoneCallsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public PhoneCalls_5.Models.Persons Persons { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Persons.Add(Persons);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
